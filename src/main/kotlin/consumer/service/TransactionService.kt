package consumer.service

import consumer.dto.TransactionMessage
import consumer.rabbit.RabbitMQConsumer
import consumer.rabbit.TransactionPoolListener
import jakarta.inject.Inject
import jakarta.inject.Singleton

@Singleton
class TransactionService {

    @Inject
    private lateinit var rabbitMQConsumer: RabbitMQConsumer<TransactionMessage>


    fun processTransactions(): List<String> {
        val messages = rabbitMQConsumer.consumeMessagesFromQueue(TransactionPoolListener.QUEUE_NAME, TransactionMessage::class)
        return messages.map { it -> it.uuid.toString() }
    }
}
