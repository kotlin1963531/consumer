package consumer.controller

import consumer.service.TransactionService
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.MediaType
import jakarta.inject.Inject

@Controller("/transaction")
class TransactionController {

    @Inject
    private lateinit var transactionService: TransactionService

    @Get(uri="/", produces=[MediaType.APPLICATION_JSON])
    fun index(): List<String> {
        return transactionService.processTransactions()
    }
}
