package consumer.rabbit

import com.rabbitmq.client.BuiltinExchangeType
import com.rabbitmq.client.Channel
import io.micronaut.rabbitmq.connect.ChannelInitializer
import org.slf4j.LoggerFactory
import java.io.IOException
import jakarta.inject.Singleton

@Singleton
class TransactionPoolListener : ChannelInitializer() {
    private val logger = LoggerFactory.getLogger(TransactionPoolListener::class.java)

    companion object {
        const val EXCHANGE_NAME = "transaction-exchange"
        const val QUEUE_NAME = "PENDING_TRANSACTIONS"
        const val ROUTING_KEY = "PENDING_TRANSACTIONS-routing-key"
    }

    @Throws(IOException::class)
    override fun initialize(channel: Channel, name: String) {
        try {
            declareExchange(channel)
            declareQueue(channel)
            bindQueueToExchange(channel)
            logger.info("RabbitMQ channel initialized successfully.")
        } catch (e: IOException) {
            handleInitializationError(e)
        }
    }

    @Throws(IOException::class)
    private fun declareExchange(channel: Channel) {
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT, true)
    }

    @Throws(IOException::class)
    private fun declareQueue(channel: Channel) {
        channel.queueDeclare(QUEUE_NAME, true, false, false, null)
    }

    @Throws(IOException::class)
    private fun bindQueueToExchange(channel: Channel) {
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, ROUTING_KEY)
    }

    private fun handleInitializationError(e: IOException) {
        logger.error("Error initializing RabbitMQ channel: {}", e.message)
        throw e
    }
}
