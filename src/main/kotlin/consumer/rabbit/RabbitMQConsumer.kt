package consumer.rabbit

import com.fasterxml.jackson.databind.ObjectMapper
import com.rabbitmq.client.GetResponse
import io.micronaut.rabbitmq.connect.ChannelPool
import jakarta.inject.Inject
import jakarta.inject.Named
import jakarta.inject.Singleton
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass

@Singleton
class RabbitMQConsumer<T : Any>(
    private val channelPool: ChannelPool,
) {

    private val log = LoggerFactory.getLogger(RabbitMQConsumer::class.java)

    @Inject
    private lateinit var objectMapper: ObjectMapper

    fun consumeMessagesFromQueue(queueName: String, typeClass: KClass<T>): List<T> {
        val messageList = mutableListOf<T>()

        channelPool.channel.use { channel ->
            while (true) {
                val response: GetResponse? = channel.basicGet(queueName, true)
                if (response == null) {
                    break
                }


                val message = deserialize(response.body, typeClass.java)
                log.info("Received message with ID: ${response.body}")
                messageList.add(message)
            }
        }

        return messageList
    }

    private fun deserialize(jsonBytes: ByteArray, typeClass: Class<T>): T {
        return objectMapper.readValue(jsonBytes, typeClass)
    }
}
