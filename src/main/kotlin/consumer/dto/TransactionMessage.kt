package consumer.dto

import io.micronaut.serde.annotation.Serdeable
import java.util.UUID

@Serdeable
data class TransactionMessage(
    val uuid: UUID,
    val type: TypeEnum,
    val amount: Double,
)

