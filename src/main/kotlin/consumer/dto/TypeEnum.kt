package consumer.dto

enum class TypeEnum {
    CARD_PAYMENT, CASH_WITHDRAWAL, TRANSFER
}
